import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)

  if (fighter) {
    const { name, health, attack, defense } = fighter;
    const imageElement = createFighterImage(fighter);
    const nameElement = createFighterInfoItemElement('name', name);
    const healthElement = createFighterInfoItemElement('health', health);
    const attackElement = createFighterInfoItemElement('attack', attack);
    const defenseElement = createFighterInfoItemElement('defense', defense);
    fighterElement.append(imageElement, nameElement, healthElement, attackElement, defenseElement);
  }

  return fighterElement;
}

function createFighterInfoItemElement( itemName, textContent ) {
  const element = createElement({
    tagName: 'p',
    className: `fighter-preview___${itemName}`
  });
  element.textContent = `${itemName.toUpperCase()}: ${textContent.toString().toUpperCase()}`;
  return element;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
