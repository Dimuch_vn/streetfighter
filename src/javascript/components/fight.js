import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {  

  function makePlayer (fighter, healthBarId) {
    return {
      health: fighter.health,
      attack: fighter.attack,
      defense: fighter.defense,
      currentHealth: fighter.health,
      healthBarId: healthBarId,
    }
  }
  
  function Delay (ms) {
    this.status = false;
    this.timerId = null;
    this.resetStatus = function () {
      this.status = false;
    }
    this.start = function() {
      this.status = true;
      this.timerId = setTimeout(this.resetStatus.bind(this), ms);
    }
    this.reset = function() {
      if (this.status) {
        clearTimeout(this.timerId);
      }      
    }   
  }

  const {
    PlayerOneAttack,
    PlayerOneBlock,
    PlayerTwoAttack,
    PlayerTwoBlock,
    PlayerOneCriticalHitCombination,
    PlayerTwoCriticalHitCombination,
  } = controls;
  const pressedKeys = new Set ();

  const playerOne = makePlayer(firstFighter, 'left-fighter-indicator');
  const playerTwo = makePlayer(secondFighter, 'right-fighter-indicator');

  const playerOneDelayCriticalHit = new Delay(10000);
  const playerTwoDelayCriticalHit = new Delay(10000); 

  const isCriticalHitCombination = (pressedKey, criticalHitCombination) => {
    const isAdjustedKey = () => criticalHitCombination.some( item => pressedKey === item);
    const isAllAdjustedKeys = () => criticalHitCombination.every( item => pressedKeys.has(item));
    return isAdjustedKey() && isAllAdjustedKeys();
  }
    
  const hit = (attackingPlayer, defendingPlayer) => {
    const damage = getDamage(attackingPlayer, defendingPlayer);
    defendingPlayer.currentHealth -= damage;
    if (defendingPlayer.currentHealth < 0) {
      defendingPlayer.currentHealth = 0;
    }
    return damage > 0;
  }

  const makeCriticalHit = (attackingPlayer, defendingPlayer, delay) => {
    if (!delay.status) {
      defendingPlayer.currentHealth -= 2*attackingPlayer.attack;
      if (defendingPlayer.currentHealth < 0) {
        defendingPlayer.currentHealth = 0;
      }
      delay.start();
      return true;
    }
    return false;
  }
    
  const updateHealthBar = (player) => {
    document.getElementById(player.healthBarId).style.width = player.currentHealth*100/player.health + '%'; 
  }

  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    const handleKeyDown = event => {
      let requestUpdateLeftHealthBar = false;
      let requestUpdateRightHealthBar = false;
      pressedKeys.add(event.code);

      if (  event.code === PlayerOneAttack 
          && !pressedKeys.has(PlayerOneBlock) 
          && !event.repeat
          && !pressedKeys.has(PlayerTwoBlock)) {
        //first fighter hit
        requestUpdateRightHealthBar = hit(playerOne, playerTwo);        
      } else if (event.code === PlayerTwoAttack 
          && !pressedKeys.has(PlayerTwoBlock) 
          && !event.repeat
          && !pressedKeys.has(PlayerOneBlock)) {
        //second fighter hit
        requestUpdateLeftHealthBar = hit(playerTwo, playerOne, pressedKeys.has(PlayerOneBlock));
      } else if (isCriticalHitCombination(event.code, PlayerOneCriticalHitCombination)) {
        //first fighter critical hit
        requestUpdateRightHealthBar = makeCriticalHit(playerOne, playerTwo, playerOneDelayCriticalHit);
      } else if (isCriticalHitCombination(event.code, PlayerTwoCriticalHitCombination)) {
        //second fighter critical hit
        requestUpdateLeftHealthBar = makeCriticalHit (playerTwo, playerOne, playerTwoDelayCriticalHit);
      }

      if (requestUpdateLeftHealthBar) {
        updateHealthBar(playerOne);
      } else if (requestUpdateRightHealthBar) {
        updateHealthBar(playerTwo);
      }

      if (playerOne.currentHealth <= 0) {
        //second fighter won
        prepareResolve();
        resolve (secondFighter);
      } else if (playerTwo.currentHealth <= 0) {
        //first fighter won
        prepareResolve();
        resolve (firstFighter);
      }

    };
        
    const handleKeyUp = function (event) {
      pressedKeys.delete(event.code);
    };
    
    const prepareResolve  = () => {
      document.removeEventListener('keydown', handleKeyDown)
      document.removeEventListener('keyup', handleKeyUp)
      playerOneDelayCriticalHit.reset();
      playerTwoDelayCriticalHit.reset();
    }

    document.addEventListener('keydown', handleKeyDown)
    document.addEventListener('keyup', handleKeyUp)

  });
}

function getChance() {
  return Math.random() + 1;
} 

export function getDamage(attacker, defender) {
  const result = getHitPower(attacker) - getBlockPower(defender)
  return result > 0 ? result : 0;  
}

export function getHitPower(fighter) {
  return fighter.attack*getChance();
}

export function getBlockPower(fighter) {
  return fighter.defense*getChance();
}

