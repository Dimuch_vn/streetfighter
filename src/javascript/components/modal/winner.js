import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  // call showModal function 

  const body = createImage(fighter);
  showModal({
    title: `${fighter.name} is winner`,
    bodyElement: body,
    onClose: function () {
        document.location.reload()
      }
  });
}

function createImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source,
    title: name,
    alt: name, 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-image-win',
    attributes
  });

  return imgElement;
};